package fr.afpa.services;

import static org.junit.Assert.assertFalse;

import org.junit.jupiter.api.Test;

import fr.afpa.metier.services.ServicePersonne;

public class ServicePersonneTest {
	
	@Test
	public void testDeletePersonne() {
		assertFalse(new ServicePersonne().deletePersonne("adm"));
		assertFalse(new ServicePersonne().deletePersonne(""));
	}

}

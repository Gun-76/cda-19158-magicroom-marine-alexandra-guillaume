package fr.afpa.services;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceAuthentification;

public class ServiceAuthentificationTest {
	
	@Test
	public void testRechercheByLoginOrNom() {
		Personne personne = new ServiceAuthentification().authentification("az", "1234");
		assertEquals("Zenina", personne.getNom());
		assertEquals(1, personne.getRole().getId());
		personne = new ServiceAuthentification().authentification("adz", "1234");
		assertNull(personne);
	}

}

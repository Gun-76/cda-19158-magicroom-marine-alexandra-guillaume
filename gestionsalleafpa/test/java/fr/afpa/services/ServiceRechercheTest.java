package fr.afpa.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;

import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceRecherche;

public class ServiceRechercheTest {
	
	@Test
	public void testRechercheByLoginOrNom() {
		List<Personne> liste = new ServiceRecherche().rechercheByLoginOrNom("login", "adm");
		assertEquals(1, liste.size());
		assertEquals("adm", liste.get(0).getAuthentification().getLogin());
		liste = new ServiceRecherche().rechercheByLoginOrNom("nom", "");
		assertFalse(liste.isEmpty());
		liste = new ServiceRecherche().rechercheByLoginOrNom("login", "");
		assertTrue(liste.isEmpty());
	}
	
	@Test
	public void testGetAllPersonnes() {
		List<Personne> liste = new ServiceRecherche().getAllPersonnes();
		assertFalse(liste.isEmpty());
	}

}

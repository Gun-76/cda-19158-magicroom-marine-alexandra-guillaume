package fr.afpa.controles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.Test;

public class ControleSaisieTest {
	

	public ControleSaisieTest() {
		super();
	}

	@Test
	public void testIsNom() {
		assertTrue(ControleSaisie.isNom("to to"));
		assertFalse(ControleSaisie.isNom("toto45"));
		assertFalse(ControleSaisie.isNom(" toto"));
		assertFalse(ControleSaisie.isNom("-toto"));
		
	}

	@Test
	public void testIsMail() {
		assertTrue(ControleSaisie.isMail("tototo@gmail.com"));
		assertTrue(ControleSaisie.isMail("to.toto@gmail.com"));
		assertFalse(ControleSaisie.isMail("tot..oto@gmail.com"));
		assertFalse(ControleSaisie.isMail("tototo@gm.ail.com"));
		assertFalse(ControleSaisie.isMail("tototo.@gmail.com"));
	}

	@Test
	public void testIsDateValide() {
		assertTrue(ControleSaisie.isDateValide("1985-01-01"));
		assertFalse(ControleSaisie.isDateValide("2005-01-01"));
		assertFalse(ControleSaisie.isDateValide("1900-01-01"));
		assertFalse(ControleSaisie.isDateValide("0d1985-01-01"));
	}

	@Test
	public void testIsNumTel() {
		assertTrue(ControleSaisie.isNumTel("0123456789"));
		assertFalse(ControleSaisie.isNumTel("01253456789"));
		assertFalse(ControleSaisie.isNumTel("012346789"));
		assertFalse(ControleSaisie.isNumTel(""));
	}

	@Test
	public void testIsNonVide() {
		assertTrue(ControleSaisie.isNonVide("eeeee"));
		assertFalse(ControleSaisie.isNonVide(""));
		assertFalse(ControleSaisie.isNonVide("  "));
		
	}

}

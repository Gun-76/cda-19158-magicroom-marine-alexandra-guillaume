package fr.afpa.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceRecherche;

/**
 * Servlet implementation class RecherchePersonne
 */
public class RecherchePersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RecherchePersonne() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "admin";

		// verifie si une personne est bien authentifié pour accéder au menu
		if (request.getSession().getAttribute("persAuth") instanceof Personne) {
			uri = "rechercheFinal.jsp";
			ServiceRecherche servRech = new ServiceRecherche();
			List<Personne> listePersRech = null;
			listePersRech = servRech.getAllPersonnes();
			request.setAttribute("listePers", listePersRech);
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else { // non authentifier retour à la page authentification
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher = null;
		String uri = "admin/acceuil/recherche";
		
		if (request.getParameter("login") != null) {
			List<Personne> listePersonne = new ServiceRecherche().rechercheByLoginOrNom("login",
					request.getParameter("login"));
			Personne personne = null;
			if (listePersonne != null && listePersonne.size() == 1) {
				personne = listePersonne.get(0);
			}

			if (personne != null) {
				uri = "pageInfoUser.jsp";
				request.setAttribute("pers", personne);
				requestDispatcher = request.getRequestDispatcher("/" + uri);
				requestDispatcher.forward(request, response);
			} else {
				response.sendRedirect(request.getContextPath() + "/" + uri);
			}
		} else {
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}	
	}
}

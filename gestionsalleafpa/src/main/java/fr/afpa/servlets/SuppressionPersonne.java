package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServicePersonne;

/**
 * Servlet implementation class SuppressionPersonne
 */
public class SuppressionPersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SuppressionPersonne() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		//verifie si une personne est bien authentifié pour accéder au menu
		if(request.getSession().getAttribute("persAuth")instanceof Personne ) {
			
			response.sendRedirect(request.getContextPath()+"/admin/acceuil");
			
		}else { // non authentifier retour à la page authentification
			
		response.sendRedirect(request.getContextPath()+"/admin");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		boolean result = new ServicePersonne().deletePersonne(request.getAttribute("login").toString());
		if(result) {
			request.setAttribute("message", "La Personne a été supprimée" );
		}else {
			request.setAttribute("message", "Erreur  la Personne n'a pas été supprimée, on ne peut pas supprimer un administrateur");
		}
		RequestDispatcher requestDispatcher;
		requestDispatcher = request.getRequestDispatcher("/suppression.jsp");
		requestDispatcher.forward(request, response);
	}

}

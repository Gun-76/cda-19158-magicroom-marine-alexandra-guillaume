package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.controles.ControleSaisie;
import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServicePersonne;
import fr.afpa.metier.services.ServiceRecherche;

/**
 * Servlet implementation class CreationUser
 */
public class CreationUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CreationUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "admin";

		// verifie si une personne est bien authentifié pour accéder au menu
		if (request.getSession().getAttribute("persAuth") instanceof Personne) {
			uri = "pageCreationUser.jsp";
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else { // non authentifier retour à la page authentification
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = null;

		boolean ok = true;
		request.setAttribute("mail", request.getParameter("mail"));
		request.setAttribute("telephone", request.getParameter("telephone"));
		request.setAttribute("login", request.getParameter("login"));
		request.setAttribute("nom", request.getParameter("nom"));
		request.setAttribute("prenom", request.getParameter("prenom"));
		request.setAttribute("mdp", request.getParameter("mdp"));
		request.setAttribute("role", request.getParameter("role"));
		request.setAttribute("fonction", request.getParameter("fonction"));
		request.setAttribute("adresse", request.getParameter("adresse"));
		request.setAttribute("date", request.getParameter("date"));
		if(request.getAttribute("role")!=null) {
		request.setAttribute("role", Integer.parseInt(request.getParameter("role")));
		}else {
			ok = false;
			request.setAttribute("roleKO", "*selectionner un role");
		}
		

		if (!ControleSaisie.isMail(request.getParameter("mail"))) {
			ok = false;
			request.setAttribute("mailKO", "*mail invalide");
		}
		if (!ControleSaisie.isNumTel(request.getParameter("telephone"))) {
			ok = false;
			request.setAttribute("telKO", "*10 chiffres sans espace");
		}
		if (!ControleSaisie.isNonVide(request.getParameter("login") ) || !new ServiceRecherche().rechercheByLoginOrNom("login", request.getParameter("login")).isEmpty()) {
			ok = false;
			request.setAttribute("loginKO", "*login déjà existant ou non valide");
		}
		if (!ControleSaisie.isNonVide(request.getParameter("mdp"))) {
			ok = false;
			request.setAttribute("mdpKO", "*mot de passe non vide");
		}
		if (!ControleSaisie.isNonVide(request.getParameter("adresse"))) {
			ok = false;
			request.setAttribute("adresseKO", "*adresse non vide");
		}
		if (!ControleSaisie.isDateValide(request.getParameter("date"))) {
			ok = false;
			request.setAttribute("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
		}
		if (!ControleSaisie.isNom(request.getParameter("prenom"))) {
			ok = false;
			request.setAttribute("prenomKO", "*uniquement des lettres sans accent");
		}
		if (!ControleSaisie.isNom(request.getParameter("nom"))) {
			ok = false;
			request.setAttribute("nomKO", "*uniquement des lettres sans accent");
		}
		
		if (!ok) {
			uri = "pageCreationUser.jsp";
			request.setAttribute("KO","erreur données formulaire");
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else {
			boolean creationOk;
			creationOk = new ServicePersonne().creationPersonne(request.getParameter("nom"), request.getParameter("prenom"), request.getParameter("mail"), request.getParameter("telephone"), request.getParameter("adresse"), request.getParameter("date"), request.getParameter("role"), request.getParameter("fonction"), request.getParameter("login"), request.getParameter("mdp"), true);
			if(creationOk) {
				request.setAttribute("message", "La personne a bien été créée");
			}else {
				request.setAttribute("message", "La personne n'a pas été créée, veuillez recommencer!");
			}
			requestDispatcher = request.getRequestDispatcher("/suppression.jsp");
			requestDispatcher.forward(request, response);
		}
		
	}

}

package fr.afpa.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.controles.ControleSaisie;
import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServicePersonne;
import fr.afpa.metier.services.ServiceRecherche;

/**
 * Servlet implementation class ModifierPersonne
 */
public class ModifierPersonne extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ModifierPersonne() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "admin";
		Personne personne = null;

		if (request.getSession().getAttribute("persAuth") instanceof Personne) {

			if (request.getAttribute("login") != null) {

				List<Personne> listePersonne = new ServiceRecherche().rechercheByLoginOrNom("login",
						request.getAttribute("login").toString());

				if (listePersonne != null && listePersonne.size() == 1) {
					personne = listePersonne.get(0);
				}

			}
			if (personne != null) {
				uri = "modificationUser.jsp";
				request.setAttribute("pers", personne);
				requestDispatcher = request.getRequestDispatcher("/" + uri);
				requestDispatcher.forward(request, response);

			}
		} else { // retour à la page authentification
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher requestDispatcher;
		String uri = null;
		
		if (request.getParameter("log") != null) {

			boolean ok = true;
			request.setAttribute("mail", request.getParameter("mail"));
			request.setAttribute("telephone", request.getParameter("telephone"));
			request.setAttribute("login", request.getParameter("log"));
			request.setAttribute("nom", request.getParameter("nom"));
			request.setAttribute("prenom", request.getParameter("prenom"));
			request.setAttribute("mdp", request.getParameter("mdp"));
			request.setAttribute("role", request.getParameter("role"));
			request.setAttribute("fonction", request.getParameter("fonction"));
			request.setAttribute("adresse", request.getParameter("adresse"));
			request.setAttribute("date", request.getParameter("date"));

			if (!ControleSaisie.isMail(request.getParameter("mail"))) {
				ok = false;
				request.setAttribute("mailKO", "*mail invalide");
			}
			if (!ControleSaisie.isNumTel(request.getParameter("telephone"))) {
				ok = false;
				request.setAttribute("telKO", "*10 chiffres sans espace");
			}

			if (!ControleSaisie.isNonVide(request.getParameter("mdp"))) {
				ok = false;
				request.setAttribute("mdpKO", "*mot de passe non vide");
			}
			if (!ControleSaisie.isNonVide(request.getParameter("adresse"))) {
				ok = false;
				request.setAttribute("adresseKO", "*adresse non vide");
			}
			if (!ControleSaisie.isDateValide(request.getParameter("date"))) {
				ok = false;
				request.setAttribute("dateKO", "*age minimum est 16 ans, age maximum 80 ans");
			}
			if (!ControleSaisie.isNom(request.getParameter("prenom"))) {
				ok = false;
				request.setAttribute("prenomKO", "*uniquement des lettres sans accent");
			}
			if (!ControleSaisie.isNom(request.getParameter("nom"))) {
				ok = false;
				request.setAttribute("nomKO", "*uniquement des lettres sans accent");
			}

			if (!ok) {
				request.setAttribute("KO", "Erreur saisie");
				doGet(request, response);
				//uri="admin/acceuil";
				//requestDispatcher = request.getRequestDispatcher("/" + uri);
				//requestDispatcher.forward(request, response);
			} else {
				uri="admin/acceuil";
				new ServicePersonne().updatePers(request.getParameter("nom"), request.getParameter("prenom"),
						request.getParameter("mail"), request.getParameter("telephone"),
						request.getParameter("adresse"), request.getParameter("date"), request.getParameter("actif"),
						request.getParameter("log"), request.getParameter("role"), request.getParameter("fonction"));

		
				response.sendRedirect(request.getContextPath() + "/" + uri);

			}
		} else {
			doGet(request, response);
		}
	}

}

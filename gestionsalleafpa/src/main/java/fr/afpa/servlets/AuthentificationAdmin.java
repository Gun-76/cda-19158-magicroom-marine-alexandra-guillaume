package fr.afpa.servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.constantes.Constantes;
import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceAuthentification;

/**
 * Servlet implementation class AuthentificationAdmin
 */
public class AuthentificationAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AuthentificationAdmin() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "authentificationAdminFinal.jsp";
		requestDispatcher = request.getRequestDispatcher("/" + uri);
		requestDispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher requestDispatcher;
		String uri;

		ServiceAuthentification servAuth = new ServiceAuthentification();
		Personne persAuth;

		uri = "authentificationAdminFinal.jsp";

		// recuperation de la personne authentifier, null si l'authenfication a échoué
		persAuth = servAuth.authentification(request.getParameter("loginAdmin"), request.getParameter("mdpAdmin"));

		if (persAuth != null && persAuth.getRole().getId() == Constantes.ID_ROLE_ADMIN) { // la pers auth est un admin
			uri = "admin/acceuil";
		} else if (persAuth != null) { // la pers auth est un utilisateur(empeche l'accès)
			request.setAttribute("KO", "désolé les utilisateurs n'ont pas accès à cette partie");
		} else { // authentification incorrecte
			request.setAttribute("KO", "Erreur login ou mot de passe incorrects ");
		}

		if ("admin/acceuil".equals(uri)) {// si l'authentification ok
			// ici session à la plce context

			request.getSession().setAttribute("persAuth", persAuth);
			response.sendRedirect(request.getContextPath() + "/" + uri);

		} else { // authentification ko
			request.getSession().setAttribute("persAuth", null);
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		}
	}

}

package fr.afpa.servlets;

import java.io.IOException;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.metier.entites.Personne;
import fr.afpa.metier.services.ServiceRecherche;

/**
 * Servlet implementation class GestionModifUser
 */
public class GestionModifUser extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GestionModifUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher requestDispatcher;
		String uri = "admin";

		// verifie si une personne est bien authentifié pour accéder au menu
		if (request.getSession().getAttribute("persAuth") instanceof Personne) {
			uri = "pageAcceuil.jsp";
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else { // non authentifier retour à la page authentification
			response.sendRedirect(request.getContextPath() + "/" + uri);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher requestDispatcher = null;
		String uri = null;

		if (request.getParameter("modifier") != null || request.getParameter("supprimer") != null) {
			if (request.getParameter("modifier") != null) {
				
				
				uri = "admin/acceuil/recherche/modifier";
				request.setAttribute("login", request.getParameter("modifier"));
				//
			} else {
				request.setAttribute("login", request.getParameter("supprimer"));
				uri = "admin/acceuil/supprimer";
			}
			requestDispatcher = request.getRequestDispatcher("/" + uri);
			requestDispatcher.forward(request, response);
		} else {
			uri = "admin/acceuil";
			response.sendRedirect(request.getContextPath() + "/" + uri);

		}

	}

}

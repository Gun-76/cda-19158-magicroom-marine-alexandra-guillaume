package fr.afpa.dao.persistance;

import java.util.logging.Level;
import java.util.logging.Logger;



import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.dao.entites.PersonneDao;
import fr.afpa.utils.HibernateUtils;

public class ServicePersonneDao {

	
	/**
	 * service de suppression d'une personne 
	 * @param personneDao : une entité PersonneDao à supprimer
	 * @return : true si la suppresion à été effectué, false sinon
	 */
	public boolean deletePersonne(PersonneDao personneDao) {
		Session s = null;
		Transaction tx=null;
		boolean retour=false;
		try {
			s = HibernateUtils.getSession();
			tx = s.beginTransaction();
			s.delete(personneDao);
			tx.commit();
			retour=true;
		}catch (Exception e) {
		Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return retour;
	}
/**
 * service de modification d'un user	
 * @param personneDao : l'entité personneDao à modifier dans la base de données 
 * @return : vrai si l'opperation a bien été effectué, faux dans le cas contraire
 */
public boolean updatePerson(PersonneDao personneDao) {
	Session s = null;
	Transaction tx=null;
	boolean retour=false;
	try {
		s = HibernateUtils.getSession();
		tx = s.beginTransaction();
		s.update(personneDao);
		tx.commit();
		retour=true;
	}catch (Exception e) {
	Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);
		
	}finally {
		if(s!=null) {
			try {
				s.close();
			} catch (Exception e) {
				Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE, null, e);
			}
		}
	}
	return retour;
}
/**
 * service de sauvegarde d'une personne 
 * @param personneDao : l'entité personneDao à créée dans la base de données
 * @return : vrai si l'opperation à bien été effectué, faux dans le cas contraire
 */
public boolean savePersonne(PersonneDao personneDao) {
	Session s = null;
	Transaction tx =null;
	boolean retour = false;
	try {
		s = HibernateUtils.getSession();
		tx = s.beginTransaction();
		s.save(personneDao.getAuthentification());
		s.save(personneDao);
		tx.commit();
		retour = true;
	}catch(Exception e) {
		Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE,null, e);
		
	}finally {
		if(s!= null) {
			try {
				s.close();
			}catch (Exception e) {
				Logger.getLogger(ServicePersonneDao.class.getName()).log(Level.SEVERE,null, e);
			}
		}
	}
	return retour;
}
}

package fr.afpa.dao.persistance;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.PersonneDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceRechercheDao {


	/**
	 * service dao qui execute la requete de recherche personneDAO via le nom ou login
	 * 
	 * @param typeRecherche : le type de recherche (login ou nom)
	 * @param valeur        : le login ou le début du nom recherché
	 * @return : une liste d'entités PersonnesDao récupéré via la requete
	 */
	public List<PersonneDao> rechercheByLoginOrNom(String typeRecherche, String valeur) {
		Session s = null;
		List<PersonneDao> listePersDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			
			if ("login".equals(typeRecherche)) {
				q = s.getNamedQuery("rechercheByLogin");
				q.setParameter("login", valeur);
			} else if ("nom".equals(typeRecherche)) {
				q = s.getNamedQuery("rechercheByNom");
				q.setParameter("nom", valeur);
			}
			if(q!=null) {
				listePersDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listePersDao;
	}

	/**
	 * service dao qui execute la requete pour avvoir toutes les  personneDAO
	 * @return : une liste d'entités PersonnesDao récupéré via la requete
	 */
	public List<PersonneDao> getAllPersonnes() {
		Session s = null;
		List<PersonneDao> listePersDao = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("getAll");
			if(q!=null) {
				listePersDao = q.list();
			}

		} catch (Exception e) {
			Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRechercheDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listePersDao;
	}

}

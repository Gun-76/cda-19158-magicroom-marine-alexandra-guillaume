package fr.afpa.dao.persistance;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.RoleDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceRoleDao {

	/**
	 * methode pour recuperer un login via le id du role
	 * 
	 * @param idRole: un entier qui represente le numero de l'identifiant
	 * @return : la liste des role avec l'identifiant donnée, l'identifiant étant
	 */
	public List<RoleDao> rechercheRoleById(int idRole) {
		Session s = null;
		List<RoleDao> listRole = null;

		try {
			s = HibernateUtils.getSession();
			Query q = null;
			q = s.getNamedQuery("rechercheRoleById");
			q.setParameter("id", idRole);
		
				listRole = q.list();
			
		} catch (Exception e) {
			Logger.getLogger(ServiceRoleDao.class.getName()).log(Level.SEVERE, null, e);

		} finally {
			if (s != null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceRoleDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return listRole;
	}
}

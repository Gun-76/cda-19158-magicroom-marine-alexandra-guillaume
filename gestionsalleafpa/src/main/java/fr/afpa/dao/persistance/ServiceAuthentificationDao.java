package fr.afpa.dao.persistance;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Session;
import org.hibernate.query.Query;

import fr.afpa.dao.entites.AuthentificationDao;
import fr.afpa.utils.HibernateUtils;

public class ServiceAuthentificationDao {

	/**
	 * demande à la bdd si le login et le mot de passe sont corrects, si oui renvoie de l'authentificationDaO correspondant
	 * @param authDaoVerif : l'authentification à vérifier
	 * @return : une entité authentificationDAO si le login et le mot de passe sont corrects, null sinon
	 */
	public AuthentificationDao authentification(AuthentificationDao authDaoVerif) {
		Session s = null;
		AuthentificationDao authDaoResultat = null;
		
		try {
			s = HibernateUtils.getSession();
			Query q = s.getNamedQuery("verifAuth");
			q.setParameter("login", authDaoVerif.getLogin());
			q.setParameter("mdp", authDaoVerif.getMdp());
			authDaoResultat = (AuthentificationDao) q.getSingleResult();
			
		}catch (Exception e) {
		Logger.getLogger(ServiceAuthentificationDao.class.getName()).log(Level.SEVERE, null, e);
			
		}finally {
			if(s!=null) {
				try {
					s.close();
				} catch (Exception e) {
					Logger.getLogger(ServiceAuthentificationDao.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
		return authDaoResultat;
	}

}

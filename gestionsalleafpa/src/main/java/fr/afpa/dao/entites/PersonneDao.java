package fr.afpa.dao.entites;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor

@Getter
@Setter
@ToString(of= {"id","nom","prenom","mail","tel","adresse","dateDeNaissance","role","fonction","authentification"})

@Entity
@Table(name = "personne")


@NamedQuery(name = "rechercheByLogin",query = "from PersonneDao where login = :login")
@NamedQuery(name = "rechercheByNom",query = "from PersonneDao where lower(nom) like lower(:nom||'%')")
@NamedQuery(name = "getAll", query = "from PersonneDao")
public class PersonneDao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "personne_generator")
	@SequenceGenerator(name = "personne_generator", sequenceName = "seq_personne",allocationSize = 1)
	@Column (name="id_personne", updatable = false, nullable = false )
	int id;
	
	@Column (name="nom", updatable = true, nullable = false,length = 50)
	String nom;
	@Column (name="prenom", updatable = true, nullable = false,length = 50)
	String prenom;
	@Column (name="mail", updatable = true, nullable = false,length = 50)
	String mail;
	@Column (name="tel", updatable = true, nullable = false,length = 50)
	String tel;
	@Column (name="adresse", updatable = true, nullable = false,length = 50)
	String adresse;
	@Column (name="date_naissance", updatable = true, nullable = false)
	LocalDate dateDeNaissance;
	@Column (name="actif", updatable = true, nullable = false)
	boolean actif;
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_role")
	RoleDao role;
	
	@ManyToOne(cascade = {CascadeType.PERSIST})
	@JoinColumn(name="id_fonction")
	FonctionDao fonction;
	
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name="login",unique = true)
	AuthentificationDao authentification;

	public PersonneDao(String nom, String prenom, String mail, String tel, String adresse, LocalDate dateDeNaissance,
			boolean actif) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.mail = mail;
		this.tel = tel;
		this.adresse = adresse;
		this.dateDeNaissance = dateDeNaissance;
		this.actif = actif;
	}
	
	
	
}

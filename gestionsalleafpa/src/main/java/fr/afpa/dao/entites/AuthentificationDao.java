package fr.afpa.dao.entites;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NamedQuery;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(of= {"login","mdp"})

@Entity
@Table(name = "authentification")
@NamedQuery(name = "verifAuth",query = "from AuthentificationDao where login = :login and mdp = : mdp")
public class AuthentificationDao {

	@Id
	@Column (name="login", updatable = false, nullable = false, length = 50 )
	String login;
	
	@Column (name="mdp", updatable = true, nullable = false,length = 50)
	String mdp;
	
	@OneToOne (mappedBy = "authentification")
	PersonneDao personne;
}

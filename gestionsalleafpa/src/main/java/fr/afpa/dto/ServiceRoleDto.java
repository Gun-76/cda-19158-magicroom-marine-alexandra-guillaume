package fr.afpa.dto;

import fr.afpa.dao.entites.RoleDao;
import fr.afpa.metier.entites.Role;

public class ServiceRoleDto {

	
	
	
	
	
	/**
	 * méthode qui permet de transformer une entité  role metier en entité
	 *  roleDao
	 * 
	 * @param  role : entité  role metier à transformer
	 * @return : une entité  roleDao correspondante à l'entite  role metier
	 */
	public static RoleDao roleMetierToRoleDao(Role role) {
		RoleDao roleDao = new RoleDao();
		roleDao.setId(role.getId());
		roleDao.setLibelle(role.getLibelle());
		
		return roleDao;
	}
	
	
	/**
	 * méthode qui permet de transformer une entité roleDao en entité role metier
	 * @param auth : entité roleDao à transformer
	 * @return : une entité role metier correspondante à l'entité roleDao
	 */
	public static Role roleDaoToRoleMetier(RoleDao roleDao) {
		Role role = new Role(roleDao.getId(), roleDao.getLibelle());
		return role;
	}

}

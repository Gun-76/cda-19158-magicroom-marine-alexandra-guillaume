package fr.afpa.dto;

import fr.afpa.dao.entites.AuthentificationDao;
import fr.afpa.dao.persistance.ServiceAuthentificationDao;
import fr.afpa.metier.entites.Authentification;
import fr.afpa.metier.entites.Personne;

public class ServiceAuthentificationDto {

	/**
	 * service authentification Dto, transforme l'entité Authentification en
	 * parametre en entité AuthentificationDAO, ensuite appel le service DAO
	 * authentification pour vérifier que l'authentification est correcte si le
	 * service DAO renvoie une AuthentificationDAO, il transforme la personne
	 * correspondant à l'authentification en entité personne et la renvoie
	 * 
	 * @param auth : authentification metier à controler
	 * @return une entité personne metier si l'authentification est correcte, null sinon
	 */
	public Personne authentification(Authentification auth) {
		Personne personneAuth = null;
		AuthentificationDao authDaoVerif = authMetierToAuthDao(auth);
		AuthentificationDao authDaoRecup = new ServiceAuthentificationDao().authentification(authDaoVerif);
		if (authDaoRecup != null) {
			personneAuth = ServicePersonneDto.personneDaoToPersonneMetier(authDaoRecup.getPersonne());
		}
		return personneAuth;
	}

	/**
	 * methode qui permet de transformer une entité authentification métier en
	 * entité authentificationDao
	 * 
	 * @param auth : entité authentification métier à transformer
	 * @return : une entité authentificationDao correspondante à l'entité
	 *         authentification métier
	 */
	public static AuthentificationDao authMetierToAuthDao(Authentification auth) {
		AuthentificationDao authDao = new AuthentificationDao();
		authDao.setLogin(auth.getLogin());
		authDao.setMdp(auth.getMdp());
		return authDao;
	}

	/**
	 * méthode qui permet de transformer une entité authentificationDao en entité
	 * authentification métier
	 * 
	 * @param auth : entité authentificationDao à transformer
	 * @return : une entité authentification métier correspondante à l'entité
	 *         authentificationDao
	 */
	public static Authentification authDaoToAuthMetier(AuthentificationDao authDao) {
		Authentification auth = new Authentification(authDao.getLogin(), authDao.getMdp());
		return auth;
	}

}

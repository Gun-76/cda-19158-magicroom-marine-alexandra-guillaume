package fr.afpa.dto;

import java.util.List;
import java.util.stream.Collectors;

import fr.afpa.dao.entites.PersonneDao;
import fr.afpa.dao.persistance.ServiceRechercheDao;
import fr.afpa.metier.entites.Personne;

public class ServiceRechercheDto {


	/**
	 * service DTO qui sert de lien entre le service metier de recherche personne
	 * via login ou nom  et le service  dao(effectue la recheche demandée), transforme la liste d'entité PersonneDao renvoyé
	 * par le DAO en liste d'entité personne metier avant de la renvoyer au service metier
	 *@param typeRecherche : le type de recherche (login ou nom)
	 * @param valeur :  le login ou le début du nom recherché
	 * @return une liste d'entités personnes metiers, correspondant à la recherche
	 */
	public List<Personne> rechercheByLoginOrNom(String typeRecherche, String valeur) {
		List<PersonneDao> listePersDao = new ServiceRechercheDao().rechercheByLoginOrNom(typeRecherche,valeur);
		List<Personne> listePers = null;
		if(listePersDao!=null) {
		 listePers = listePersDao.stream().map(ServicePersonneDto::personneDaoToPersonneMetier)
				.collect(Collectors.toList());
		}
		return listePers;
	}

	/**
	 * service DTO qui sert de lien entre le service metier de recuperation de toutes les  personnes
	 et le service  dao, transforme la liste d'entité PersonneDao renvoyé
	 * par le DAO en liste d'entité personne metier avant de la renvoyer au service metier

	 * @return une liste d'entités personnes metiers
	 */
	public List<Personne> getAllPersonnes() {
		List<PersonneDao> listePersDao = new ServiceRechercheDao().getAllPersonnes();
		List<Personne> listePers = null;
		if(listePersDao!=null) {
		 listePers = listePersDao.stream().map(ServicePersonneDto::personneDaoToPersonneMetier)
				.collect(Collectors.toList());
		}
		return listePers;
	}

}

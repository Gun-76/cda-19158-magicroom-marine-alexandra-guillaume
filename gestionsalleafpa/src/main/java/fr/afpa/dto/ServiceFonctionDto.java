package fr.afpa.dto;

import fr.afpa.dao.entites.FonctionDao;
import fr.afpa.metier.entites.Fonction;

public class ServiceFonctionDto {

	
	
	
	
	
	
	
	
	/**
	 * méthode qui permet de transformer une entité fonction metier en entité
	 * fonctionDao
	 * 
	 * @param fonction : entité fonction metier à transformer
	 * @return : une entité fonctionDao correspondante à l'entite fonction metier
	 */
	public static FonctionDao fonctionMetierToFonctionDao(Fonction fonction) {
		FonctionDao fonctionDao = new FonctionDao();
		fonctionDao.setId(fonction.getId());
		fonctionDao.setLibelle(fonction.getLibelle());
	
		return fonctionDao;
	}

	/**
	 * methode qui permet de transformer une entité fonctionDao en entité fonction
	 * metier
	 * 
	 * @param fonctionDao : entité fonctionDao à transformer
	 * @return : une entité fonction metier correspondante à l'entité fonctionDao
	 */
	public static Fonction fonctionDaoToFonctionMetier(FonctionDao fonctionDao) {
		Fonction fonction = new Fonction(fonctionDao.getId(), fonctionDao.getLibelle());
		return fonction;
	}

}

package fr.afpa.constantes;

public class Constantes {
	
public final static int ID_ROLE_ADMIN=1;
public final static int ID_ROLE_UTILISATEUR=2;

public final static int ID_FONCTION_DIRECTEUR=1;
public final static int ID_FONCTION_FORMATTEUR=2;
public final static int ID_FONCTION_STAGIAIRE=3;
public final static int ID_FONCTION_SECRETAIRE=4;
public final static int ID_FONCTION_VISITEUR=5;
public final static int ID_FONCTION_INTERVENANT=6;

}

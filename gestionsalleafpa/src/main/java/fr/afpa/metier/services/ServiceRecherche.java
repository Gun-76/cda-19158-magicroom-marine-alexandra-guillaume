package fr.afpa.metier.services;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.dto.ServiceRechercheDto;
import fr.afpa.metier.entites.Personne;

public class ServiceRecherche {
	
	
	
	/**
	 * Service de recherche de personne via le login ou via le nom (insensible à la casse et comparaison des 1ers lettres) 
	 *@param typeRecherche : le type de recherche (login ou nom)
	 * @param valeur :  le login ou le début du nom recherché
	 * @return une liste d'entité métier personne,  correspondant à la recherche
	 */
	public List<Personne> rechercheByLoginOrNom(String typeRecherche,String valeur) {
		List<Personne> liste = new ArrayList<Personne>();
		liste = new ServiceRechercheDto().rechercheByLoginOrNom(typeRecherche,valeur);
		return liste;
	}

	/**
	 * Service qui renvoie la liste de tous les personnnes
	 * @return une liste d'entité métier personne,  correspondant à tous les personnes
	 */
	public List<Personne> getAllPersonnes() {
		List<Personne> liste = new ArrayList<Personne>();
		liste = new ServiceRechercheDto().getAllPersonnes();
		return liste;
	}

}

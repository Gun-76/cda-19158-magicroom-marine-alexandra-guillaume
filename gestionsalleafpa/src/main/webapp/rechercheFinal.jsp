<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="fr.afpa.constantes.Constantes"%>
<%@page import="fr.afpa.metier.entites.Personne"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Recherche Utilisateur</title>
<link href="/gestionsalleafpa/css/cssRecherche.css"
	rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.persAuth.role.id==Constantes.ID_ROLE_ADMIN}">
		
			<div class="container">


				<a href="${pageContext.request.contextPath}/admin/deconnexion">Se
					deconnecter</a> <a
					href="${pageContext.request.contextPath}/admin/acceuil">|
					Retour vers la page d'acceuil |</a>

				<h2>Rechercher Utilisateur</h2>

				<form method="post">

					<input type="text" class="champRecherche" list="users" name="login"
						required>

					<datalist id="users">

						<c:forEach var="pers" items="${listePers}">

							<option value="${pers.authentification.login}">

								${pers.authentification.login} ${pers.nom} ${pers.prenom}

								${pers.fonction.libelle}</option>
						</c:forEach>

					</datalist>

					<button type="submit" class="searchButton" value="Rechercher">
						Voir</button>

				</form>
			</div>
			
		</c:when>
		<c:otherwise>
			<c:redirect url="admin"/>
		</c:otherwise>
	</c:choose>
	
</body>
</html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>admin</title>
<link href ="/gestionsalleafpa/css/cssAuthentification.css" rel="stylesheet">
</head>
<body>

<flex-container>

            <label id="labelInfo">Connexion Administrateur</label>
            
           <!-- ---------------------------------------------------- getAttibute ici Version JAVA request.getAttribute("KO")!=null -->
            <c:if test ="${requestScope.KO!=null}">
            	<em class ="erreur">
            	${requestScope.KO}
            	</em>
            
            </c:if>
            
           <!--  VERSION JAVA
           <% if (request.getAttribute("KO")!=null) {%>
				<em class="erreur"> <%=request.getAttribute("KO") %></em>
				<%}  %> -->
        				
				
            <form action="admin" method="post">
                <label for="LoginLabel">Login :</label>
                <br> 
                <input name="loginAdmin" type="text" required/> 
                <br> 
                <br> 
                <br> 
                <label for="MdpLabel">Mot de passe :</label>
                <br> 
                <input name="mdpAdmin" type="password" required/> 
                <br> 
                <br> 
                <br> 
                <button type="submit" value="ValiderB" >Valider</button>
            </form>
 </flex-container>
</body>
</html>
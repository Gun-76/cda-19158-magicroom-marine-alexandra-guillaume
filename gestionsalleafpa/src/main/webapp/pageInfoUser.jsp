<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="fr.afpa.metier.entites.Personne"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Informations</title>
<link
	href="/gestionsalleafpa/css/cssUser.css"
	rel="stylesheet">
</head>
<body>

	<div class="container">
		<div class="entete">
			<!--Lien pour la deconnexion ici-->
				<a  href="${pageContext.request.contextPath}/admin/deconnexion">Se deconnecter</a>
				
			<h2>Information Utilisateur</h2>
		</div>
		<!--Ligne UNE-->
		<div class="ligne">
			<form action="gestion" method="post">
				<!--COLONNE GAUCHE-->
				<div class="colonne">
					<!--partie nom-->
					<div class="colonneGauche">
						<label>Nom :</label>
					</div>
					<div class="colonneDroite">
						<label name="nom">${pers.nom}</label>
					</div>
					<!--partie prenom-->
					<div class="colonneGauche">
						<label>Prenom :</label>
					</div>
					<div class="colonneDroite">
						<label prenom="prenom">${pers.prenom}</label>
					</div>
					<!--partie mail-->
					<div class="colonneGauche">
						<label>Mail :</label>
					</div>
					<div class="colonneDroite">
						<label mail="mail">${pers.mail}</label>
					</div>
					<!--partie telephone-->
					<div class="colonneGauche">
						<label>Telephone :</label>
					</div>
					<div class="colonneDroite">
						<label name="telephone">${pers.tel}</label>
					</div>
					<!--partie adresse-->
					<div class="colonneGauche">
						<label>Adresse :</label>
					</div>
					<div class="colonneDroite">
						<label name="adresse">${pers.adresse}</label>
					</div>
					<!--partie date-->
					<div class="colonneGauche">
						<label for="naissance">Date de naissance :</label>
					</div>
					<div class="colonneDroite">
						<label name="date">${pers.dateDeNaissance}</label>
					</div>
				</div>
				<!--COLONNE DROITE-->
				<div class="colonne">
					<!--partie login-->
					<div class="colonneGauche">
						<label>Login :</label>
					</div>
					<div class="colonneDroite">
						<label name="login">${pers.authentification.login}</label>
					</div>
					<!--partie role-->
					<div class="colonneGauche">
						<label>Role :</label>
					</div>
					<div class="colonneDroite">
						<label name="role">${pers.role.libelle}</label>
					</div>
					<!--partie fonction-->
					<div class="colonneGauche">
						<label>Fonction :</label>
					</div>
					<div class="colonneDroite">
						<label name="fonction">${pers.fonction.libelle}</label>
					</div>

					<!--colonnes vides pour le positionnement-->
					<div class="colonneGauche">
						<label></label>
					</div>
					<div class="colonneDroite">
						<label></label>
					</div>
					
					
					<!--partie BOUTTON MODIFICATION USER-->
					<div class="colonneGauche">
						<label></label>
					</div>
					<div class="colonneDroite">
						<button type="submit" name="modifier"  value="${pers.authentification.login}">Modifier</button>
					</div>

					<!--partie BOUTTON Supprimer ajouter un pop up!-->
					<div class="colonneGauche">
						<label></label>
					</div>
					<div class="colonneDroite">
						<button type="submit" name="supprimer"  value="${pers.authentification.login}">Supprimer</button>
					</div>


					<!--partie BOUTTON ANNULER-->
					<div class="colonneGauche">
						<label></label>
					</div>
					<div class="colonneDroite">
						<button type="submit" name="annuler" value="annuler">Annuler</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
<%@page import="fr.afpa.constantes.Constantes"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CreationUtilisateur</title>
<link href ="/gestionsalleafpa/css/cssUser.css" rel="stylesheet">
</head>
<body>
<c:choose>
		<c:when test="${sessionScope.persAuth.role.id==Constantes.ID_ROLE_ADMIN}">
  <div class="container">
            <div class="entete">
                
                <!--Lien pour la deconnexion ici-->   
               <a href="${pageContext.request.contextPath}/admin/deconnexion">Se deconnecter</a>
                <h2>Creation d'un nouvel utilisateur</h2>
                
                
            </div>
            
                        
            <div class="ligne">
            
            <!--Debut de FORM --> 
                <form method="post">
                
                    <!--COLONNE GAUCHE-->
                    <div class="colonne">
                    
                        <!--partie nom-->
                        <div class="colonneGauche"> 
                            <label>Nom :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="nom" value="${nom}" type="text" required="">
                             <br>
                            <em>${nomKO}</em> 
                        </div>
                        
                        <!--partie prenom-->
                        <div class="colonneGauche">
                            <label>Prenom :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="prenom" value="${prenom }" type="text" required="">
                           	<br>
                            <em>${prenomKO}</em>
                        </div>
                        
                        <!--partie mail-->
                        <div class="colonneGauche">
                            <label>Mail :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="mail" value="${ mail }" type="text" required="">
                            <br>
                            <em>${mailKO}</em>
                            </div>
                        
                        <!--partie telephone-->
                        <div class="colonneGauche">
                            <label>Telephone :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="telephone" value="${telephone }" type="text" required="">
                             <br>
                            <em>${telKO}</em>
                        </div>
                        
                        <!--partie adresse-->
                        <div class="colonneGauche">
                            <label>Adresse :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="adresse" value=" ${ adresse }" type="text" required="">
                            <br>
                            <em>${adresseKO}</em>
                        </div>
                        
                        
                        <!--partie date-->
                        <div class="colonneGauche">
                            <label for="naissance">Date de naissance :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input type="date" id="naissance" name="date"  required="" value="${ date }" min="1900-01-01" max="">
                             <br>
                            <em>${dateKO}</em>
                        </div>
                    </div>
                    
                    
                    <!--COLONNE DROITE-->
                    <div class="colonne">
                    
                        <!--partie login-->
                        <div class="colonneGauche"> 
                            <label>Login :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="login" value="${ login }" type="text" required="">
                              <br>
                            <em>${loginKO}</em>
                        </div>
                        
                        <!--partie mot de passe-->
                        <div class="colonneGauche"> 
                            <label>Mot de passe :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input name="mdp" type="password" value="${ mdp }" required=""> 
                              <br>
                            <em>${mdpKO}</em>
                        </div>
                        
                        <!--partie role-->
                        <div class="colonneGauche"> 
                            <label>Role :</label>
                        </div>
                        <div class="colonneDroite"> 
                            <input type="radio" name="role" value="${Constantes.ID_ROLE_ADMIN}" 
                            	<c:if test="${role==Constantes.ID_ROLE_ADMIN}">checked</c:if>><label>administrateur</label>
                            <br>
                            <input type="radio" name="role" value="${Constantes.ID_ROLE_UTILISATEUR}"
                            	<c:if test="${role==Constantes.ID_ROLE_UTILISATEUR}">checked</c:if>><label>utilisateur simple</label>                   
                            <br>
                             <em>${roleKO}</em>
                             <br>
                        </div>
                        
                        
                        <!--partie Fonction a voir comment integrer d'autres fonction via java -->
                        
                        <div class="colonneGauche"> 
                            <label>Fonction :</label>
                        </div>
                        <div class="colonneDroite"> 
                        
                            <select name="fonction" id="selectFonction" required="">
                                <option value="">--Veuillez choisir une fonction--</option>
                              	<option value="${Constantes.ID_FONCTION_DIRECTEUR}">Directeur</option>
                                <option value="${Constantes.ID_FONCTION_SECRETAIRE}">Secretaire</option>
                                <option value="${Constantes.ID_FONCTION_FORMATTEUR}">Formateur</option>
                                <option value="${Constantes.ID_FONCTION_STAGIAIRE}">Stagiaire</option>
                                <option value="${Constantes.ID_FONCTION_INTERVENANT}">Intervenant</option>
                                <option value="${Constantes.ID_FONCTION_VISITEUR}">Visiteur</option>
                            </select>                             
                        </div>
                       
                      
                       <!--colonnes vides pour le positionnement-->
                          <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <label></label>
                        </div>
                        
                        <!--partie BOUTTON VALIDER-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                            <button type="submit" value="Valider">Valider</button>
                        </div>
                        
                        
                        <!--partie BOUTTON ANNULER retour vers l'acceuil � integrer-->
                        <div class="colonneGauche"> 
                            <label></label>
                        </div>
                        <div class="colonneDroite"> 
                       <a href="${pageContext.request.contextPath}/admin/acceuil">
									<button type="button" value="Annuler">Annuler</button>
								</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
  		</c:when>
		<c:otherwise>
			<c:redirect url="admin" />
		</c:otherwise>
	</c:choose>      
</body>
</html>
<%@page import="fr.afpa.metier.entites.Personne"%>
<%@page import="fr.afpa.constantes.Constantes"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Modification</title>
<link href="/gestionsalleafpa/css/cssUser.css"
	rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.persAuth.role.id==Constantes.ID_ROLE_ADMIN}">
		
		
			<div class="container">
				<div class="entete">

				<a href="${pageContext.request.contextPath}/admin/deconnexion">Se
						deconnecter</a>

					<h2>Modification de l'utilisateur</h2>
				</div>
				<div class="ligne">
					<!--Debut de FORM -->

					<form
						action="${pageContext.request.contextPath}/admin/acceuil/recherche/modifier"
						method="post">

						<!--COLONNE GAUCHE-->
						<div class="colonne">
							<!--partie nom-->
							<div class="colonneGauche">
								<label>Nom :</label>
							</div>
							<div class="colonneDroite">
								<input name="nom" type="text" value=${pers.nom} required>
								<br>
								<em>${nomKO}</em>
							</div>
							<!--partie prenom-->
							<div class="colonneGauche">
								<label>Prenom :</label>
								
							</div>
							<div class="colonneDroite">
								<input name="prenom" type="text" value=${pers.prenom} required>
								<br>
								<em>${prenomKO}</em>
							</div>
							<!--partie mail-->
							<div class="colonneGauche">
								<label>Mail :</label>
							</div>
							<div class="colonneDroite">
								<input name="mail" type="text" value=${pers.mail} required>
								<br>
								<em>${mailKO}</em>
							</div>
							<!--partie telephone-->
							<div class="colonneGauche">
								<label>Telephone :</label>
							</div>
							<div class="colonneDroite">
								<input name="telephone" type="text" value=${pers.tel } required>
								<br>
								<em>${telKO}</em>
							</div>

							<!--partie adresse-->
							<div class="colonneGauche">
								<label>Adresse :</label>
							</div>
							<div class="colonneDroite">
								<input name="adresse" type="text" value=${pers.adresse} required>
								<br>
								<em>${adresseKO}</em>
							</div>

							<!--partie date-->
							<div class="colonneGauche">
								<label for="naissance">Date de naissance :</label>
							</div>
							<div class="colonneDroite">
								<input type="date" id="naissance" name="date"
									value=${pers.dateDeNaissance } min="1900-01-01" max="" required>
									<br>
									<em>${dateKO}</em>
							</div>
						</div>
						<!--COLONNE DROITE-->
						<div class="colonne">
							<!--partie login-->
							<div class="colonneGauche">
								<label>Login :</label>
							</div>
							<div class="colonneDroite">
								<label>${pers.authentification.login}</label>
							</div>
							<!--partie mot de passe-->
							<div class="colonneGauche">
							
								<label>Mot de passe :</label>
							</div>
							<div class="colonneDroite">
								<input name="mdp" type="password" required
									value=${pers.authentification.mdp}>
									<br>
									<em>${mdpKO}</em>
							</div>
							<!--partie role-->
							<div class="colonneGauche">
								<label>Role :</label>
							</div>

						
							<div class="colonneDroite">

								<input type="radio" name="role" value="${Constantes.ID_ROLE_ADMIN}"
									<c:if test="${pers.role.id==Constantes.ID_ROLE_ADMIN}"> checked </c:if>> <label>administrateur</label>

								<br> <input type="radio" name="role" value="${Constantes.ID_ROLE_UTILISATEUR}"
									<c:if test="${pers.role.id==Constantes.ID_ROLE_UTILISATEUR}"> checked </c:if>> <label>utilisateur
									simple</label> <br>

							</div>
							<div class="colonneGauche">
								<label>Actif :</label>
							</div>

							<!-- Script ICI POUR SELECTIONNER si personne active -->



							<div class="colonneDroite">
								<input type="radio" name="actif" value="true"
									<c:if test="${pers.actif}"> checked </c:if>>
								<label>oui</label> <br> <input type="radio" name="actif"
									value="false" <c:if test="${not pers.actif}"> checked </c:if>>
								<label>non</label> <br>
							</div>

							<!--partie Fonction a voir comment integrer d'autres fonction via java -->
							<div class="colonneGauche">
								<label>Fonction :</label>
							</div>
							<div class="colonneDroite">
								<select name="fonction" id="selectFonction" required>
									<option value=${pers.fonction.id}>
										${pers.fonction.libelle}</option>
									<option value="${Constantes.ID_FONCTION_DIRECTEUR}">Directeur</option>
									<option value="${Constantes.ID_FONCTION_SECRETAIRE}">Secretaire</option>
									<option value="${Constantes.ID_FONCTION_FORMATTEUR}">Formateur</option>
									<option value="${Constantes.ID_FONCTION_STAGIAIRE}">Stagiaire</option>
									<option value="${Constantes.ID_FONCTION_INTERVENANT}">Intervenant</option>
									<option value="${Constantes.ID_FONCTION_VISITEUR}">Visiteur</option>
								</select>
							</div>

							<!--colonnes vides pour le positionnement-->
							<div class="colonneGauche">
								<label></label>
							</div>
							<div class="colonneDroite">
								<label></label>
							</div>
							<!--partie BOUTTON VALIDER-->
							<div class="colonneGauche">
								<label></label>
							</div>
							<div class="colonneDroite">
								<button type="submit" name="log"
									value=${pers.authentification.login}>Valider</button>
							</div>
							<!--partie BOUTTON ANNULER retour vers l'acceuil � integrer-->
							<div class="colonneGauche">
								<label></label>
							</div>
							<div class="colonneDroite">
							
								<a href="${pageContext.request.contextPath}/admin/acceuil">
									<button type="button" value="Annuler">Annuler</button>
								</a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</c:when>
		
		
		<c:otherwise>
			<c:redirect url="admin" />
		</c:otherwise>
	</c:choose>
</body>
</html>
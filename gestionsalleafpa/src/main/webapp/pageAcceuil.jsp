<%@page import="org.omg.CORBA.portable.ApplicationException"%>
<%@page import="fr.afpa.metier.entites.Personne"%>
<%@page import="fr.afpa.constantes.Constantes"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri ="http://java.sun.com/jsp/jstl/core" prefix="c" %>
 
 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Acceuil</title>
<link href ="/gestionsalleafpa/css/cssPageAcceuilChoix.css" rel="stylesheet">
</head>
<body>
	<c:choose>
		<c:when test="${sessionScope.persAuth.role.id==Constantes.ID_ROLE_ADMIN}">

 <flex-container> 
           
            <form method="post">
            
          
				 <label id="labelInfo">Bonjour  ${sessionScope.persAuth.nom} ${sessionScope.persAuth.prenom}</label>      
                <br> 
                <a href="acceuil/creation"><button type="button" value="NewUser"> Cr�er un Nouvel Utilisateur</button></a>
                <br> 
                <br> 
                <br> 
                <a href="acceuil/recherche"><button type="button" value="GestionUserexistant">Gestion d'un Utilisateur Existant</button></a>
                <br> 
                <br> 
                 <br> 
                 <br> 
                <br> 
                <a href="deconnexion"><button type="button" value="Deconnexion" >D�connexion</button></a>
            </form>
        </flex-container>
        		</c:when>
		<c:otherwise>
			<c:redirect url="admin" />
		</c:otherwise>
	</c:choose>
</body>
</html>
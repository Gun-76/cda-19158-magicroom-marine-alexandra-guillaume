------------------------------------------------------------
--        Script Postgre 
------------------------------------------------------------



------------------------------------------------------------
-- Table: type_salle
------------------------------------------------------------
CREATE TABLE public.type_salle(
	id_type   SERIAL NOT NULL ,
	libelle   VARCHAR (50) NOT NULL  ,
	CONSTRAINT type_salle_PKEY PRIMARY KEY (id_type)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: batiment
------------------------------------------------------------
CREATE TABLE public.batiment(
	id_batiment   SERIAL NOT NULL ,
	nom           VARCHAR (50) NOT NULL  ,
	CONSTRAINT batiment_PKEY PRIMARY KEY (id_batiment)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: salle
------------------------------------------------------------
CREATE TABLE public.salle(
	numero        INT  NOT NULL ,
	nom           VARCHAR (50) NOT NULL ,
	surface       NUMERIC ,
	capacite      INT   ,
	etage         INT   ,
	actif         BOOL   ,
	id_type       INT  NOT NULL ,
	id_batiment   INT  NOT NULL  ,
	CONSTRAINT salle_PKEY PRIMARY KEY (numero)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: type_materiel
------------------------------------------------------------
CREATE TABLE public.type_materiel(
	id_type   SERIAL NOT NULL ,
	libelle   VARCHAR (50) NOT NULL  ,
	CONSTRAINT type_materiel_PKEY PRIMARY KEY (id_type)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: reservation
------------------------------------------------------------
CREATE TABLE public.reservation(
	id_reservation    SERIAL NOT NULL ,
	date_debut        DATE  NOT NULL ,
	date_fin          DATE  NOT NULL ,
	nom_reservation   VARCHAR (50) NOT NULL ,
	numero            INT  NOT NULL  ,
	CONSTRAINT reservation_PKEY PRIMARY KEY (id_reservation)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: role
------------------------------------------------------------
CREATE TABLE public.role_personne(
	id_role   SERIAL NOT NULL ,
	libelle   VARCHAR (50) NOT NULL  ,
	CONSTRAINT role_PKEY PRIMARY KEY (id_role)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: fonction
------------------------------------------------------------
CREATE TABLE public.fonction(
	id_fonction   SERIAL NOT NULL ,
	libelle       VARCHAR (50) NOT NULL  ,
	CONSTRAINT fonction_PKEY PRIMARY KEY (id_fonction)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: personne_archive
------------------------------------------------------------
CREATE TABLE public.personne_archive(
	id_personne      SERIAL NOT NULL ,
	nom              VARCHAR (50) NOT NULL ,
	prenom           VARCHAR (50) NOT NULL ,
	mail             VARCHAR (50) NOT NULL ,
	tel              VARCHAR (50) NOT NULL ,
	adresse          VARCHAR (50) NOT NULL ,
	date_naissance   DATE   ,
	actif            BOOL   ,
	fonction         VARCHAR (50)  ,
	date_archive     DATE  NOT NULL  ,
	CONSTRAINT personne_archive_PKEY PRIMARY KEY (id_personne)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: posseder
------------------------------------------------------------
CREATE TABLE public.posseder(
	id_type    INT  NOT NULL ,
	numero     INT  NOT NULL ,
	quantite   INT  NOT NULL  ,
	CONSTRAINT posseder_PKEY PRIMARY KEY (id_type,numero)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: personne
------------------------------------------------------------
CREATE TABLE public.personne(
	id_personne      SERIAL NOT NULL ,
	nom              VARCHAR (50) NOT NULL ,
	prenom           VARCHAR (50) NOT NULL ,
	mail             VARCHAR (50) NOT NULL ,
	tel              VARCHAR (50) NOT NULL ,
	adresse          VARCHAR (50) NOT NULL ,
	date_naissance   DATE   ,
	actif            BOOL   ,
	login            VARCHAR (50) NOT NULL UNIQUE,
	id_role          INT  NOT NULL ,
	id_fonction      INT  NOT NULL  ,
	CONSTRAINT personne_PKEY PRIMARY KEY (id_personne)
)WITHOUT OIDS;


------------------------------------------------------------
-- Table: authentification
------------------------------------------------------------
CREATE TABLE public.authentification(
	login         VARCHAR (50) NOT NULL ,
	mdp           VARCHAR (50) NOT NULL ,
	CONSTRAINT authentification_PKEY PRIMARY KEY (login)
)WITHOUT OIDS;

------------------------------------------------------------
-- Constraintes cl� etrang�res
------------------------------------------------------------


ALTER TABLE public.salle
	ADD CONSTRAINT salle_type_salle0_FK
	FOREIGN KEY (id_type)
	REFERENCES public.type_salle(id_type);

ALTER TABLE public.salle
	ADD CONSTRAINT salle_batiment1_FK
	FOREIGN KEY (id_batiment)
	REFERENCES public.batiment(id_batiment);

ALTER TABLE public.reservation
	ADD CONSTRAINT reservation_salle0_FK
	FOREIGN KEY (numero)
	REFERENCES public.salle(numero);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_authentificaion0_FK
	FOREIGN KEY (login)
	REFERENCES public.authentification(login);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_role0_FK
	FOREIGN KEY (id_role)
	REFERENCES public.role_personne(id_role);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_fonction0_FK
	FOREIGN KEY (id_fonction)
	REFERENCES public.fonction(id_fonction);


------------------------------------------------------------
-- contraintes format donn�es
------------------------------------------------------------
ALTER TABLE public.personne
	ADD CONSTRAINT personne_date_naissance
	CHECK (cast (to_char(date_naissance,'yyyy') as integer)
	between cast (to_char(now(),'yyyy') as integer)-88 
and cast (to_char(now(),'yyyy') as integer)-16);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_desactivation_admin
	CHECK ((id_role=1 and actif=true) or id_role!=1);

ALTER TABLE public.personne
	ADD CONSTRAINT personne_tel
	CHECK (tel ~ E'^\\d{10}$');

ALTER TABLE public.personne
	ADD CONSTRAINT personne_prenom
	CHECK (prenom ~ E'^[A-Za-z \\-���]+$');

ALTER TABLE public.personne
	ADD CONSTRAINT personne_nom
	CHECK (nom ~ E'^[A-Za-z \\-���]+$');


------------------------------------------------------------
-- creation sequence
------------------------------------------------------------

Create sequence seq_personne owned by personne.id_personne;
Create sequence seq_role owned by role_personne.id_role;
Create sequence seq_fonction owned by fonction.id_fonction;


------------------------------------------------------------
-- trigger suppression personne
------------------------------------------------------------

create or replace function delete_utilisateur() returns trigger as
$body$
declare 
fonction varchar;
begin
	select libelle into fonction from fonction f where old.id_fonction=f.id_fonction;
	if(old.id_role=1) then
		raise exception 'un administrateur ne peux pas �tre desactiver';
	else
		INSERT INTO public.personne_archive
			(nom, prenom, mail, tel, adresse, date_naissance, actif, fonction, date_archive)
				VALUES(old.nom, old.prenom, old.mail, old.tel, old.adresse, old.date_naissance, old.actif, fonction, now());
		return old;
	end if;
end
$body$
language plpgsql;

create trigger trigger_delete_utilisateur
before delete on personne
for each row execute procedure delete_utilisateur();

------------------------------------------------------------
-- insertion donn�es
------------------------------------------------------------

insert into role_personne values (nextval('seq_role'),'ADMINISTRATEUR');
insert into role_personne values (nextval('seq_role'),'UTILISATEUR');

insert into fonction values (nextval('seq_fonction'),'DIRECTEUR');
insert into fonction values (nextval('seq_fonction'),'FORMATEUR');
insert into fonction values (nextval('seq_fonction'),'STAGIAIRE');
insert into fonction values (nextval('seq_fonction'),'SECRETAIRE');
insert into fonction values (nextval('seq_fonction'),'VISITEUR');
insert into fonction values (nextval('seq_fonction'),'INTERVENANT');


insert into authentification values ('adm','admin');
insert into authentification values ('mb','1234');
insert into authentification values ('az','1234');
insert into authentification values ('gg','1234');
insert into authentification values ('mz','1234');
insert into authentification values ('ff','1234');

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1952-05-01', 'admi@gmail.com', 'superadmin', 'superadmin', '0320101010', 'adm', 1, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1984-05-01', 'mb@gmail.com', 'Benjira', 'Mohammed', '0320101010', 'mb', 2, 2);
INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'az@gmail.com', 'Zenina', 'Alexandra', '0320101010', 'az', 3, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Grauwin', 'Guillaume', '0320101010', 'gg', 4, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Zimmer', 'Marine', '0320101010', 'mz', 6, 1);

INSERT INTO public.personne
(id_personne, actif, adresse, date_naissance, mail, nom, prenom, tel, login, id_fonction, id_role)
VALUES(nextval('seq_personne'), true, '03 rue truc Lille', '1990-05-01', 'gg@gmail.com', 'Fayak', 'Fayaz', '0320101010', 'ff', 5, 2);








Projet : Gestion de salle AFPA
Auteur : Marine , Alexandra , Guillaume

Technologies et outils utilis�s : Java / Maven / Lombok / Hibernate / JEE /  Gitkraken / Bitbucket / JMerise / PgAdmin 4 / DBeaver 

D�marche � suivre :

1) Sur pgAdmin ou autre syst�me de gestion de base de donn�es, cr�er un r�le de connexion.

nom = admin 
mot de passe = 1234 

2) Sur pgAdmin ou autre syst�me de gestion de base de donn�es, cr�er une nouvelle base de donn�es PostgreSQL(avec l'user admin)

3) Executer le script "script_creation.sql"( qui se trouve dans le dossier BDD)

4) Demarrer l'application avec le serveur TomCat (version 8.5)

	A l'execution du programme vous commencer sur la page d'authentification utilisateur l'url de cette page est http://localhost:8080/gestionsalleafpa/

	Pour acceder a la pasge d'authentification de l'administrateur il faut ajouter /admin � l'url (ou cliquer sur le lien connexion administrateur).
	
	Accedez � cette page et connecter vous avec l'utilisateur : adm et le mot de passe : admin.
	
5) Une fois connect� vous allez pouvoir 

	Cr�er des utilisateur et des administrateur

	Consulter les informations des utilisateurs

	Modifier les information des utilisateurs

	Supprimer des utilisateurs

	